import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './pages/users/users.component';
import { UserComponent } from './pages/user/user.component';

const appRoutes: Routes = [
    { path: '', component: UsersComponent },
    { path: 'user/:id', component: UserComponent },
    { path: '**', redirectTo: '', pathMatch: 'full' }
]


export const APP_ROUTES = RouterModule.forRoot(appRoutes, { useHash: true });
