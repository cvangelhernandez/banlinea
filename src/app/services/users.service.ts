import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICES } from '../config/config';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) {
    console.log('servicio listo para se usado')
   }

   getUsers(){
     let url = URL_SERVICES + '/users'
     return this.http.get(url)
   }

   getUser(id){
    let url = URL_SERVICES + '/users/' + id
    return this.http.get<User>(url)
   }
}
