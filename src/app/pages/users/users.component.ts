import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { filter, flatMap, map } from 'rxjs/operators'
import { User } from '../../models/user.model';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  public users: any;
  public term: any;

  constructor(private _User: UsersService) { }

  ngOnInit() {
    this.getUsers()
  }

  getUsers() {
    this._User.getUsers().subscribe(
      res => {
        console.log(res)
        this.users = res;
      },
      error => {
        console.log(error);
      }
    )
  }


}
