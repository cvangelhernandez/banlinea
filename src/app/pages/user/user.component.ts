import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from '../../services/users.service';
import { User } from '../../models/user.model';

declare var $:any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  public id: number;
  public user: User;

  constructor( 
    private _activateRoute: ActivatedRoute,
    private _User: UsersService
  ) {
    this.user = new User(
      0,
        '',
        '',
        '',
        {
          street: '',
          suite: '',
          city: '',
          zipcode: '',
          geo: {
            lat: '',
            lng: '',
          }
        },
        '',
        '',
        {
          name: '',
          catchPhrase: '',
          bs: 'string',
        }
    )
    this._activateRoute.params.subscribe(params => {
      this.id = params['id']
    });
  }

  ngOnInit() {
    this.getUser()
    $(".form").on('click', function(){
      $(this).addClass('active');
    });
  }

  getUser(){
    this._User.getUser(this.id).subscribe(
      res => {
        this.user = res;
        console.log(this.user)
      },
      err => {
        console.log(err)
      }
    )
  }

}
